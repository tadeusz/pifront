import './App.css';
import React from 'react';
import {GcodeExecutionControl} from './components/GcodeExecutionControl';

/*

        Frontend for the pigcd
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


function App() {
  return (
    <div className="App">
      <header className="App-header">
        PiGcd by Tadeusz Puźniakowski
      </header>



      <GcodeExecutionControl />

      <h2>Quick pick examples</h2>
      <pre>
        ; go to 0,0,0 on classical machine
        G28
        G92X0Y0Z0
        G0ZZ-33.6069
        G92X0Y0Z0
      </pre>
      <pre>
        ; go to 0,0,0 starting at Z=0
        G28
        G0Z0
        G92X0Y0Z0
      </pre>
    </div>
  );
}

export default App;
