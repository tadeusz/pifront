/*

        Frontend for the pigcd
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import './GcodeExecutionControl.css';
import React from 'react';
import { GcodeApi } from '../services/GcodeApi.js';
import Button from "@mui/material/Button";
import TextareaAutosize from "@mui/material/TextareaAutosize";

export class GcodeExecutionControl extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            gcode: "",
            log: {},
            logText: "",
            status: "",
            error: "",
            executing: false,
            hold: false,
            exec_index: 0
        };
        this.onGcodeTextValueChange = this.onGcodeTextValueChange.bind(this);
        this.tick = this.tick.bind(this);
        this.sendStatusRequest = this.sendStatusRequest.bind(this);
        this.sendGcodeRequest = this.sendGcodeRequest.bind(this);
        this.sendHoldRequest = this.sendHoldRequest.bind(this);
        this.sendResumeRequest = this.sendResumeRequest.bind(this);
        this.sendCancelRequest = this.sendCancelRequest.bind(this);
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            5000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    onGcodeTextValueChange(e) {
        this.setState({ gcode: e.target.value });
    }

    tick() {
        var pThis = this;
        GcodeApi.updateStatus(this.state, (statusUpdate, err) => {
            if (err) {
                pThis.setState({ log: err });
            } else {
                pThis.setState({ logText: GcodeApi.getResultLogString(this.state) });
                pThis.setState(statusUpdate);
            }
        });
    }
    sendStatusRequest() {
        GcodeApi.send("?", (statusUpdate, id, err) => this.setState(statusUpdate));
    }
    sendGcodeRequest() {
        GcodeApi.send(this.state.gcode, (statusUpdate, id, err) => this.setState(statusUpdate));
    }
    sendHoldRequest() {
        GcodeApi.send("!", (statusUpdate, id, err) => this.setState(statusUpdate));
    }
    sendResumeRequest() {
        GcodeApi.send("~", (statusUpdate, id, err) => this.setState(statusUpdate));
    }
    sendCancelRequest() {
        GcodeApi.send("&", (statusUpdate, id, err) => this.setState(statusUpdate));
    }

    render() {
        return (
            <div>
                <h2>Gcode controls</h2>
                <Button disabled={this.state.executing} onClick={this.sendGcodeRequest}>Send</Button>
                <Button onClick={this.sendStatusRequest}>Status</Button>

                <Button disabled={!((!this.state.hold) && (this.state.executing))} onClick={this.sendHoldRequest}>Hold</Button>
                <Button disabled={!((this.state.hold) && (this.state.executing))} onClick={this.sendResumeRequest}>Resume</Button>
                <Button disabled={!this.state.executing} onClick={this.sendCancelRequest}>Terminate</Button>
                <h2>Gcode execution</h2>
                <TextareaAutosize value={this.state.gcode} onChange={this.onGcodeTextValueChange}></TextareaAutosize>
                <TextareaAutosize value={this.state.logText} readOnly></TextareaAutosize>
                <h2>Gcode status</h2>
                <span >{this.state.status}</span>
            </div>
        );
    }
}
