import {JsonAPIConfig} from '../utils/JsonAPIConfig';


function defaultCallback(newStatus, id, error) {
};
function defaultStatusCallback(newStatus, err) {
};
function send(gcode, callback) {
  if (callback === undefined) callback = defaultCallback;
  fetch(JsonAPIConfig.exec, {
    headers: { 'Content-Type': 'text/plain; charset=utf-8' },
    method: 'POST',
    body: gcode
  })
    .then((response) => response.json())
    .then(resp => {
      resp.log = {};
      callback(resp, resp.id);
    })
    .catch((err) => callback(undefined, undefined, err));
};
function updateStatus(status, callback) {
  if (callback === undefined) callback = defaultStatusCallback;
  var key;
  for (key = 0; status.log[key] !== undefined; key++) {
  }
  fetch(
    `${JsonAPIConfig.gcodeStatus}/${Math.max(0, key - 1)}`,
    { headers: { 'Content-Type': 'text/plani; charset=utf-8' }, method: 'GET' })
    .then((response) => response.json())
    .then(resp => {
      if (resp.id !== status.id) {
        resp.log = {};
      } else if (resp.log !== undefined) {
        var l = status.log;
        for (const elem of resp.log) {
          l[elem.i] = elem;
        }
        resp.log = l;
      }
      callback(resp);
    })
    .catch((err) => callback(undefined, err));
};
function getResultLogString(status) {
  var resultString = 'EXECUTION LOG:';
  var holdStatus = (status.hold) ? 'ON HOLD' : '...';
  try {
    for (var key = 0; status.log[key] !== undefined; key++) {
      var value = status.log[key];
      resultString = resultString + '\n[' + key + ']' + value.command + '; ' +
        ((value.log === undefined) ? holdStatus : value.log);
    }
  } catch (e) {
  }
  return resultString;
};


export const GcodeApi = {
  send: send,
  updateStatus: updateStatus,
  getResultLogString: getResultLogString
};




